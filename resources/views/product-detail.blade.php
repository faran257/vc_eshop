@extends('layouts.master')

@section('title','Product')

@section('content')
<div class="container">
		<div class="card">
			<div class="container-fliud">
				<div class="wrapper row">
					<div class="preview col-md-6">
						
						<div class="preview-pic tab-content">
						  <div class="tab-pane active" id="pic-1"><img src="{{$products->image}}" /></div>
						</div>
						<ul class="preview-thumbnail nav nav-tabs">
						  <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="{{$products->image}}" /></a></li>
						  <li><a data-target="#pic-2" data-toggle="tab"><img src="{{$products->image}}" /></a></li>
						  <li><a data-target="#pic-3" data-toggle="tab"><img src="{{$products->image}}" /></a></li>
						  <li><a data-target="#pic-4" data-toggle="tab"><img src="{{$products->image}}" /></a></li>
						  <li><a data-target="#pic-5" data-toggle="tab"><img src="{{$products->image}}" /></a></li>
						</ul>
						
					</div>
					<div class="details col-md-6">
						<h3 class="product-title">{{$products->name}}</h3>
						<div class="rating">
							<div class="stars">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star"></span>
								<span class="fa fa-star"></span>
							</div>
							<span class="review-no">41 reviews</span>
						</div>
						<p class="product-description">{{$products->description}}</p>
						<h4 class="price">current price: <span>Rs = {{$products->prices[0]->sell_price}}</span></h4>
						<p class="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p>
					
						<h5 class="colors">Available Colors:
							<span class="color" style="background-color:{{$products->colors[0]->color}};"></span>
						</h5>
						<div class="action">
							<button class="add-to-cart btn btn-default" type="button">add to cart</button>
							<button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection



