@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row">
       <div class="col-md-12">
           <a href="{{route('product.create')}}"><button class="btn  btn-success btn-md pull-right">Add Product</button></a>
            @if(session()->has('message'))
              <div class="alert alert-success">
               {{session()->get('message')}}
              </div>
            @endif
           <div class="card">
              <div class="card-header">Products</div>
              <div class="card-body"> 
                <table class="table">
                   <thead>
                     <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                   </thead>
                   @foreach ( $products as $product)
                   <tbody>
                      <tr>
                         <td>{{$product->id}}</td>
                         <td>{{$product->name}}</td>
                         <td><img src="{{$product->image}}" alt="" height="50" width="50"></td>
                         <td>{{$product->description}}</td>
                         <td>
                            <a href="{{route('product.show',$product->id)}}"><button class="btn btn-info btn-sm">More</button></a>
                            <a href="{{route('product.edit',$product->id )}}"><button class="btn btn-success btn-sm">Edit</button></a>
                            <a href=""><button class="btn btn-danger btn-sm">Dell</button></a>
                        </td>
                      </tr>
                   </tbody>
                   @endforeach
                </table>
              </div>
           </div>
       </div>
       
   </div>
</div>

@endsection
