@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row">
       <div class="col-md-12">
           <div class="card">
               <div class="card-header">Edit Product Form</div>
               <div class="card-body"> 
                    <form action="{{route('product.update',$products->id)}}" method="POST" enctype="multipart/form-data">
                     {{csrf_field()}}
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name"  placeholder="Enter name" class="form-control" value="{{$products->name}}">
                        </div>
                        <div class="form-group">
                            <label for="">Image</label>
                            <input type="file" name="image"  class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <input type="text" name="description"  placeholder="Enter description" class="form-control"  value="{{$products->description}}">
                        </div>
                        <div class="form-group">
                            <label for="">Category</label>
                            <input type="text" name="category"  placeholder="Enter category" class="form-control"  value="{{$products->categories[0]->category}}">
                        </div>
                        <div class="form-group">
                            <label for="">Brand</label>
                            <input type="text" name="brand"  placeholder="Enter brand" class="form-control"  value="{{$products->brands[0]->brand}}">
                        </div>
                        <div class="form-group">
                            <label for="">Sell Price</label>
                            <input type="text" name="sell_price"  placeholder="Enter sellprice" class="form-control" value="{{$products->prices[0]->sell_price}}">
                        </div>
                        <div class="form-group">
                            <label for="">Purchase Price</label>
                            <input type="text" name="purchase_price"  placeholder="Enter purchaseprice" class="form-control" value="{{$products->prices[0]->porchase_price}}">
                        </div>
                        <div class="form-group">
                            <label for="">Product Color</label>
                              <select name="color" id="" class="form-control">
                                <option value="">--choose color--</option>
                                <option value="black">black</option>
                                <option value="yellow">yellow</option>
                                <option value="green">green</option>
                                <option value="pink">pink</option>
                                <option value="red">red</option>
                                <option value="blue">blue</option>
                                <option value="gray">gray</option>
                              </select>
                        </div>
                        <div class="form-group">
                            <label for="">Product Size</label>
                              <select name="size" id="" class="form-control">
                                <option value="">--choose size--</option>
                                <option value="xsamll">xsmall</option>
                                <option value="small">small</option>
                                <option value="medium">medium</option>
                                <option value="large">large</option>
                                <option value="xlarage">xlarage</option>
                              </select>
                        </div>
                        <button class="btn btn-info bg-md" type="submit">Create Product</button>
                    </form>
               </div>
           </div>
       </div>
    </div>
</div>
@endsection