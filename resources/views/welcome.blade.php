@extends('layouts.master')

@section('title','Home')

@section('content')
  <!--================Home Banner Area =================-->
  <section class="home_banner_area mb-40">
     @include('page-contents.home-banner')
  </section>
  <!--================End Home Banner Area =================-->

  <!-- Start feature Area -->
  <section class="feature-area section_gap_bottom_custom">
     @include('page-contents.feature-area')
  </section>
  <!-- End feature Area -->

  <!--================ Feature Product Area =================-->
  <section class="feature_product_area section_gap_bottom_custom">
      @include('page-contents.feature-product')
  </section>
  <!--================ End Feature Product Area =================-->


  <!--================ New Product Area =================-->
  <section class="new_product_area section_gap_top section_gap_bottom_custom">
       @include('page-contents.new-product-area')
  </section>
  <!--================ End New Product Area =================-->

  <!--================ Inspired Product Area =================-->
  <section class="inspired_product_area section_gap_bottom_custom">
       @include('page-contents.inspired-product')
  </section>
  <!--================ End Inspired Product Area =================-->


 @endsection