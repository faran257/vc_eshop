<?php
  
namespace App\Traits;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
  
trait ImageTrait {
  
    /**
     * @param Request $request
     * @return $this|false|string
     */
    public function imagePhoto($request)
    {
        if ( ! $request->hasFile($this->image)) {
            return false;
        }
        $avatar = $request->file($this->image);
        $filename = time(). '.'.$avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300,300)->save(public_path('upload/'.$filename));
        $file_path = '/upload/'.$filename;
        return $file_path;
    }
  
}