<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\ImageTrait;
use App\Product;
use App\Brand;
use App\Category;
use App\ProductColor;
use App\ProductPrice;
use App\ProductSize;

class ProductController extends Controller
{
    use ImageTrait;
    protected $image = 'image';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('admin.products.product',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['image'] = $this->imagePhoto($request);
       $products = Product::create($input);
       if ($products) {
           Brand::create(['product_id'=>$products->id,'brand'=>$request->brand]);
           Category::create(['product_id'=>$products->id,'category'=>$request->category]);
           ProductPrice::create([
               'product_id'=>$products->id,
               'sell_price'=>$request->sell_price,
               'purchase_price'=>$request->purchase_price]);
            ProductSize::create(['product_id'=>$products->id,'size'=>$request->size]);
            ProductColor::create(['product_id'=>$products->id,'color'=>$request->color]);
           return redirect('/home')->with('success','record created successfully.'); 
          
       } else {
           return false;
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $products = Product::where('id',$id)->with('brands','categories','colors','prices','sizes')->first();
        // return view('admin.products.read',compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::with('brands','categories','colors','prices','sizes')->first();
        return view('admin.products.edit',compact('products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $input['image'] = $this->imagePhoto($request);
        $products = Product::where('id',$id)->update($input);
       if ($products) {
           Brand::create(['product_id'=>$id,'brand'=>$request->brand]);
           Category::create(['product_id'=>$id,'category'=>$request->category]);
           ProductPrice::create([
               'product_id'=>$id,
               'sell_price'=>$request->sell_price,
               'purchase_price'=>$request->purchase_price]);
            ProductSize::create(['product_id'=>$id,'size'=>$request->size]);
            ProductColor::create(['product_id'=>$id,'color'=>$request->color]);
           return redirect('/product')->with('message','record created successfully.'); 
          
       } else {
        return redirect('/product')->with('error','something wrong.'); 
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
