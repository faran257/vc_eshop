<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('brands','categories','colors','prices','sizes')->get();
        return view('shop',compact('products'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productDetail($id)
    {
        $products = Product::where('id',$id)->with('brands','categories','colors','prices','sizes')->first();
        return view('product-detail',compact('products'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getApi()
    {
        $products = Product::with('brands','categories','colors','prices','sizes')->get();
        return response()->json(['products',$products],200);
    }
}
