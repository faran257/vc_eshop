<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image', 'description',
    ];

     /**
     * Get category with product
     */
    public function categories()
    {
        return $this->hasMany(Category::class);
    }
    /**
     * Get brands with product
     */
    public function brands()
    {
        return $this->hasMany(Brand::class);
    }
    /**
     * Get colors with product
     */
    public function colors()
    {
        return $this->hasMany(ProductColor::class);
    }
     /**
     * Get prices with product
     */
    public function prices()
    {
        return $this->hasMany(ProductPrice::class);
    }
    /**
     * Get size with product
     */
    public function sizes()
    {
        return $this->hasMany(ProductSize::class);
    }
}
