<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'color', 
    ];
    /**
     * Get Product
     *
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
